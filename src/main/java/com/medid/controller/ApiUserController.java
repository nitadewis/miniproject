package com.medid.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.medid.model.Biodata;
import com.medid.model.ResetPassword;
import com.medid.model.Role;
import com.medid.model.User;
import com.medid.repository.BiodataRepository;
import com.medid.repository.ResetPasswordRepository;
import com.medid.repository.RoleRepository;
import com.medid.repository.UserRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/login")
public class ApiUserController {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BiodataRepository biodataRepository;
	
	
	@Autowired
	private ResetPasswordRepository resetPasswordRepository;
	
	@Autowired
	private RoleRepository roleRepository;

	@GetMapping("/")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("login");
		return view;
	}

	@GetMapping("/role")
	public ResponseEntity<List<Role>> getAllUser(){
		try {
			List<Role> role = this.roleRepository.findAll();
			return new ResponseEntity<>(role,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("add/biodata")
	public ResponseEntity<Object> saveBiodata(@RequestBody Biodata biodata) {
		Date date = new Date();
		biodata.setCreatedOn(date);
		biodata.setIsDelete(false);
		Biodata biData = this.biodataRepository.save(biodata);
		if(biData.equals(biodata)) {
			return new ResponseEntity<>("save Succesfull", HttpStatus.OK); 
		} else {
			return new ResponseEntity<>("save failed", HttpStatus.BAD_REQUEST);
		} 
	}
	
	@PostMapping("add/user")
	public ResponseEntity<Object> saveUser(@RequestBody User user) {
		Long biodata_id = this.biodataRepository.getMaxBiodataId();
		Date date = new Date();
		user.setBiodataId(biodata_id);
		user.setCreatedBy(biodata_id);
		user.setCreatedOn(date);
		user.setIsDelete(false);
		
		
		User userdata = this.userRepository.save(user);
		
		Optional<Biodata> bidata = this.biodataRepository.findById(biodata_id);
		Biodata bi = bidata.get();
		
		Long user_id = this.userRepository.getMaxUserId(biodata_id);
		
		bi.setCreatedBy(user_id);
		this.biodataRepository.save(bi);
		if(userdata.equals(user)) {
			return new ResponseEntity<>("save Succesfull", HttpStatus.OK); 
		} else {
			this.biodataRepository.deleteById(user_id);
			return new ResponseEntity<>("save failed", HttpStatus.BAD_REQUEST);
		} 
	}

	

	@PostMapping("/logincheck")
	public ResponseEntity<Object> getLogin(@RequestBody User user) {
		Date date = new Date();
		int loginAttempt = 1;
		String email = user.getEmail().toLowerCase();
		String password = user.getPassword();
		Optional<User> checkEmail = this.userRepository.findEmail(email);
		if (checkEmail.isPresent()) {
			Optional<User> loginCheck = this.userRepository.findEmailAndPassword(email, password);
			User cUser = checkEmail.get();
			if (loginCheck.isPresent()) {
				User cUserLogin = loginCheck.get();
				loginAttempt = 0;
				cUserLogin.setLastLogin(date);
				cUserLogin.setLoginAttempt(loginAttempt);
				this.userRepository.save(cUser);
				return new ResponseEntity<>(loginCheck, HttpStatus.OK);
			} else {
				loginAttempt += cUser.getLoginAttempt();
				cUser.setLoginAttempt(loginAttempt);
				this.userRepository.save(cUser);
				return new ResponseEntity<>("wrongpass", HttpStatus.OK);
			}

		} else {
			return new ResponseEntity<>("emailnotfound", HttpStatus.OK);
		}
	}

	@GetMapping("/checkemail/{email}")
	public ResponseEntity<Object> getEmail(@PathVariable("email") String email) {
		String find = email.toLowerCase();
		Optional<User> checkEmail = this.userRepository.findEmail(find);
		if (checkEmail.isPresent()) {
			return new ResponseEntity<>(checkEmail, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("false", HttpStatus.OK);
		}
	}

	@PatchMapping("/updatedata/{email}")
	public ResponseEntity<Object> updateUser(@RequestBody User user, @PathVariable("email") String email) {
		Date date = new Date();
		String find = email.toLowerCase();
		Optional<User> userData = this.userRepository.findEmail(find.trim());
		if (userData.isPresent()) {
			User cUser = userData.get();
			ResetPassword Rp = new ResetPassword();
			Rp.setOldPassword(cUser.getPassword());
			Rp.setNewPassword(user.getPassword());
			Rp.setResetFor("forgot password");
			Rp.setIsDelete(false);
			Rp.setCreatedBy(cUser.getId());
			Rp.setCreatedOn(date);
			this.resetPasswordRepository.save(Rp);
			
			cUser.setPassword(user.getPassword());
			cUser.setModified_by(cUser.getId());
			cUser.setModifiedOn(date);
			this.userRepository.save(cUser);
			return new ResponseEntity<>("Update Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

}
