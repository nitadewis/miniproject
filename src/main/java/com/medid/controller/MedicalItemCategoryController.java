package com.medid.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.medid.model.MedicalItemCategory;
import com.medid.repository.MedicalItemCategoryRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/category/")
public class MedicalItemCategoryController {
	@Autowired
	private MedicalItemCategoryRepository medicalItemCategoryRepository;
	
	@GetMapping("/")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("categoryProductKesehatan");
		return view;
	}
	

	@GetMapping("/searchcategory/{keyword}")
	public ResponseEntity<List<MedicalItemCategory>> getCategoryByName(@PathVariable("keyword") String keyword) {
			List<MedicalItemCategory> category = this.medicalItemCategoryRepository.searchCategory(keyword);
			return new ResponseEntity<>(category, HttpStatus.OK);
	}
	
	@GetMapping("categorypage")
	public ResponseEntity<Map<String, Object>> getAllCategory(
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "2") int size
			)
	{
		try {
			List<MedicalItemCategory> category = new ArrayList<MedicalItemCategory>();
			Pageable paging = PageRequest.of(page, size);
			
			Page<MedicalItemCategory> pageTuts = medicalItemCategoryRepository.orderbynameASC(paging);
	
			category = pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("category", category);
			response.put("currentPage", pageTuts.getNumber());
		    response.put("totalItems", pageTuts.getTotalElements());
		    response.put("totalPages", pageTuts.getTotalPages());
			
		      return new ResponseEntity<>(response,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("categorypagedesc")
	public ResponseEntity<Map<String, Object>> getAllCategoryDesc(
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "2") int size
			)
	{
		try {
			List<MedicalItemCategory> category = new ArrayList<MedicalItemCategory>();
			Pageable paging = PageRequest.of(page, size);
			
			Page<MedicalItemCategory> pageTuts = medicalItemCategoryRepository.orderbynameDesc(paging);
	
			category = pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("category", category);
			response.put("currentPage", pageTuts.getNumber());
		    response.put("totalItems", pageTuts.getTotalElements());
		    response.put("totalPages", pageTuts.getTotalPages());
			
		      return new ResponseEntity<>(response,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PatchMapping("delete/{id}")
	public ResponseEntity<Object> deleteCategory(@PathVariable("id") Long id){
		Date date = new Date();
		Optional<MedicalItemCategory> categoryData = this.medicalItemCategoryRepository.findById(id);
		if(categoryData.isPresent()) {
			MedicalItemCategory medical = categoryData.get();
			medical.setIsDelete(true);
			medical.setDeletedOn(date);
			this.medicalItemCategoryRepository.save(medical);
			return new ResponseEntity<>("Delete Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PatchMapping("update/{id}")
	public ResponseEntity<Object> updateCategory(@RequestBody MedicalItemCategory medic, @PathVariable("id") Long id){
		try {
			Date date = new Date();
			String name = medic.getName().trim();
			Optional<MedicalItemCategory> checkName = this.medicalItemCategoryRepository.checkName(name);
			if(!checkName.isPresent() && !name.isEmpty()) {
				Optional<MedicalItemCategory> update = this.medicalItemCategoryRepository.findById(id);
				MedicalItemCategory med = update.get();
				med.setModifiedOn(date);
				med.setName(name);
				this.medicalItemCategoryRepository.save(med);
				return new ResponseEntity<>("Save Succesfull", HttpStatus.OK); 
			}
			else {
				return new ResponseEntity<>("duplicate", HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>("save failed", HttpStatus.BAD_REQUEST);
		}
		
	}
	
	//edit
		@GetMapping("/confirmDelete/{id}") 
		public ResponseEntity<List<MedicalItemCategory>> getCategoryById(@PathVariable("id") Long id) {
			try {
				Optional<MedicalItemCategory> category = this.medicalItemCategoryRepository.checkId(id);
				if(category.isPresent()) {
					ResponseEntity rest = new ResponseEntity<>(category, HttpStatus.OK);
					return rest;
				} else {
					return ResponseEntity.notFound().build();
				}	
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		}
	
	
	@PostMapping("add/{name}")
	public ResponseEntity<Object> saveCategory(@PathVariable("name") String name) {	
		MedicalItemCategory medical = new MedicalItemCategory();
		Date date = new Date();
		medical.setName(name.trim().toLowerCase());
		medical.setCreatedOn(date);
		medical.setIsDelete(false);
		Optional<MedicalItemCategory> checkName = this.medicalItemCategoryRepository.checkName(name.trim());
		if(!checkName.isPresent() && !name.isEmpty()) {
			MedicalItemCategory categoryData = this.medicalItemCategoryRepository.save(medical);
			if(categoryData.equals(medical)) {
				return new ResponseEntity<>("Save Succesfull", HttpStatus.OK); 
			} else {
				return new ResponseEntity<>("save failed", HttpStatus.BAD_REQUEST);
			}	
		}
		else {
			return new ResponseEntity<>("duplicate", HttpStatus.OK);
		}
	}
	
	

}
