package com.medid.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.medid.model.Token;
import com.medid.model.User;
import com.medid.repository.TokenRepository;
import com.medid.repository.UserRepository;
import com.medid.service.MyEmailService;
import com.medid.service.OtpService;

@RestController
@CrossOrigin("*")
public class OtpController {

	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	public OtpService otpService;
	
	@Autowired
	private TokenRepository tokenRepository;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	public MyEmailService myEmailService;
	

	@GetMapping("/generateOtp/{email}")
	public @ResponseBody String generateOtp(@PathVariable("email") String email) {
		Date batas = new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(10));
		Date curDate = new Date();
		
//		int otp = otpService.generateOTP(email);
		Random random = new Random();
		int otp = 100000 + random.nextInt(90000);
		logger.info("OTP : " + otp);
		//ngetrim email
		String emailSend = email.trim().toLowerCase();
		
		Optional<User> checkEmail = this.userRepository.findEmail(emailSend);
		Optional<Token> changeExpired = this.tokenRepository.findEmail(emailSend);
		if(changeExpired.isPresent()) {
			Token update = changeExpired.get();
			if(checkEmail.isPresent()) {
				update.setId(changeExpired.get().getId());
				update.setModified_by(checkEmail.get().getId());
			} 
			update.setExpired(true);
			update.setModifiedOn(curDate);
			this.tokenRepository.save(update);
		} 
		
		Token token = new Token();
		//token khusus untuk forget password
		if(checkEmail.isPresent()) {
			token.setUserId(checkEmail.get().getId());
			token.setCreatedBy(checkEmail.get().getId());
			token.setUsedFor("forgot password");
		} else {
			token.setUsedFor("register");
		}
		//token untuk pendaftar baru
		token.setToken(Integer.toString(otp));
		token.setEmail(emailSend);
		token.setCreatedOn(curDate);
		token.setExpiredOn(batas);
		token.setExpired(false);
	
		this.tokenRepository.save(token);
		
		myEmailService.sendOtpMessage(emailSend, "OTP", String.valueOf(otp));
		final String SUCCESS = "OTP is send";
		return SUCCESS;
	}

	@GetMapping("/validateOtp")
	public @ResponseBody String validateOtp( @RequestParam("otpnum") int otpnum, @RequestParam("email") String email) {
		Date date = new Date();
		final String expired = "expired";
		final String FAIL = "invalid";
		
		email = email.trim().toLowerCase();
		

		logger.info(" Otp Number : " + otpnum);
		Long id = this.tokenRepository.getMaxid(email);
			if (otpnum >= 0) {
				Optional<Token> token = this.tokenRepository.findData(email, Integer.toString(otpnum));
				Date expiredData = token.get().getExpiredOn();
				if(token.isPresent()) {
					Token tokenData = token.get();
					int serverOtp = Integer.parseInt(token.get().getToken());
					Optional<User> checkEmail = this.userRepository.findEmail(email);
					if (token.get().isExpired() == false) {
						if (expiredData.after(date)) {
						if(checkEmail.isPresent()) {
							if (serverOtp > 0) {
								tokenData.setDeletedBy(checkEmail.get().getId());
								tokenData.setDeletedOn(date);
								tokenData.setModifiedOn(date);
								tokenData.setModified_by(checkEmail.get().getId());
								tokenData.setExpired(true);
								tokenData.setIsDelete(true);
								this.tokenRepository.save(tokenData);
								return ("valid");
								} else {
									return "token is null";
								}
						} else {
						return ("valid");
						}
						
						} else {
						tokenData.setDeletedBy(checkEmail.get().getId());
						tokenData.setDeletedOn(date);
						tokenData.setModifiedOn(date);
						tokenData.setModified_by(checkEmail.get().getId());
						tokenData.setExpired(true);
						tokenData.setIsDelete(true);
						this.tokenRepository.save(tokenData);
						return expired;
					} 
					} else {
						return expired;
					}
					 
					} else {
						return FAIL;
					}
		} else {
			return "otpnum is empty";
		}
	}

}
