package com.medid.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.medid.model.ResetPassword;

public interface ResetPasswordRepository extends JpaRepository<ResetPassword, Long> {

}
