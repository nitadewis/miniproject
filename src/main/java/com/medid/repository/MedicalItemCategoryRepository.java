package com.medid.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.medid.model.MedicalItemCategory;

public interface MedicalItemCategoryRepository extends JpaRepository<MedicalItemCategory, Long> {
	@Query("FROM MedicalItemCategory ORDER BY Name ASC")
	public List<MedicalItemCategory> OrderByName();
	
	@Query("FROM MedicalItemCategory WHERE IsDelete = FALSE AND LOWER(Name) = LOWER(?1)")
	public Optional<MedicalItemCategory> checkName(String name);
	
	@Query("FROM MedicalItemCategory WHERE IsDelete = FALSE AND Id = ?1")
	public Optional<MedicalItemCategory> checkId(Long id);
	
	@Query(value="SELECT * FROM m_medical_item_category WHERE is_delete = false ORDER BY name",
			countQuery = "SELECT count (*) FROM m_medical_item_category WHERE is_delete = false GROUP BY name",
			nativeQuery = true)
	public Page<MedicalItemCategory> orderbynameASC(Pageable pageable);
	
	@Query(value="SELECT * FROM m_medical_item_category WHERE is_delete = false ORDER BY name DESC",
			countQuery = "SELECT count (*) FROM m_medical_item_category WHERE is_delete = false GROUP BY name",
			nativeQuery = true)
	public Page<MedicalItemCategory> orderbynameDesc(Pageable pageable);
	
	@Query("FROM MedicalItemCategory WHERE IsDelete = FALSE AND lower(name) LIKE lower(concat('%',?1,'%'))")
	public List<MedicalItemCategory> searchCategory(String keyword);

}
