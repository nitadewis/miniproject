package com.medid.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.medid.model.Biodata;
import com.medid.model.User;

public interface BiodataRepository extends JpaRepository<Biodata, Long> {
	@Query("SELECT MAX(id) as maxid FROM Biodata")
	Long getMaxBiodataId();
	
	
}
