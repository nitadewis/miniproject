package com.medid.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.medid.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	@Query("FROM User WHERE email = ?1 AND IsDelete = 'false'")
	Optional<User> findEmail(String email);
	
	@Query("FROM User WHERE email = ?1 AND password = ?2 AND IsDelete = false")
	Optional<User> findEmailAndPassword(String email, String password);
	
	@Query("SELECT MAX(id) as maxid FROM User WHERE Id = ?1")
	Long getMaxUserId(Long id);
	
	
	
}
