package com.medid.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.medid.model.Token;

public interface TokenRepository extends JpaRepository<Token, Long>{
	@Query("SELECT MAX(id) as maxid FROM Token WHERE email = ?1 ")
	Long getMaxid(String email);
	
	@Query("FROM Token WHERE email = ?1 AND token = ?2 ")
	Optional<Token> findData(String email, String token);
	
	@Query("FROM Token WHERE email = ?1 AND isExpired = false")
	Optional<Token> findEmail(String email);


}
