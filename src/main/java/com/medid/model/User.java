package com.medid.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.sun.istack.NotNull;


@Entity
@Table(name = "m_user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	//punya biodata
	@ManyToOne
	@JoinColumn(name="biodata_id", insertable = false, updatable = false)
	public Biodata biodata;
	
	@Column(name="biodata_id")
	private Long BiodataId;
	
	@ManyToOne
	@JoinColumn(name="role_id", insertable = false, updatable = false)
	public Role role;
	
	@Column(name="role_id")
	private Long RoleId;
	
	@Column(name="email")
	private String email;
	
	@Column(name="password")
	private String password;
	
	@Column(name = "login_attempt")
	private int LoginAttempt;
   
	@NotNull
	@Column(name="is_locked")
	private boolean IsLocked;
	
	@Column(name="last_login")
	private Date LastLogin;
	
	@Column(name="created_by")
	private Long CreatedBy;
	
	@Column(name="created_on")
	private Date CreatedOn;
	
	@Column(name="modified_by")
	private Long modified_by;
	
	@Column(name="modified_on")
	private Date ModifiedOn;
	
	@Column(name="deleted_by")
	private Long DeletedBy;
	
	@Column(name="deleted_on")
	private Date DeletedOn;
	
	@NotNull
	@Column(name="is_delete")
	private boolean IsDelete;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getBiodataId() {
		return BiodataId;
	}

	public void setBiodataId(Long biodataId) {
		BiodataId = biodataId;
	}

	public Long getRoleId() {
		return RoleId;
	}

	public void setRoleId(Long roleId) {
		RoleId = roleId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getLoginAttempt() {
		return LoginAttempt;
	}

	public void setLoginAttempt(int loginAttempt) {
		LoginAttempt = loginAttempt;
	}

	public boolean isIsLocked() {
		return IsLocked;
	}

	public void setIsLocked(boolean isLocked) {
		IsLocked = isLocked;
	}

	public Date getLastLogin() {
		return LastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		LastLogin = lastLogin;
	}

	public Long getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Long createdBy) {
		CreatedBy = createdBy;
	}

	public Date getCreatedOn() {
		return CreatedOn;
	}

	public void setCreatedOn(Date createdOn) {
		CreatedOn = createdOn;
	}

	public Long getModified_by() {
		return modified_by;
	}

	public void setModified_by(Long modified_by) {
		this.modified_by = modified_by;
	}

	public Date getModifiedOn() {
		return ModifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		ModifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return DeletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		DeletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return DeletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		DeletedOn = deletedOn;
	}

	public boolean isIsDelete() {
		return IsDelete;
	}

	public void setIsDelete(boolean isDelete) {
		IsDelete = isDelete;
	}
	
	
	
}
