package com.medid.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name = "t_token")
public class Token {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@Column(name="email")
	private String email;
	
	@Column(name="user_id")
	private Long userId;

	@Column(name="token")
	private String token;
	
	@Column(name="expired_on")
	private Date ExpiredOn;
	
	@Column(name="is_expired")
	private boolean isExpired;
	
	@Column(name="used_for")
	private String usedFor;
	
	@Column(name="created_by")
	private Long CreatedBy;
	
	@Column(name="created_on")
	private Date CreatedOn;
	
	@Column(name="modified_by")
	private Long modified_by;
	
	@Column(name="modified_on")
	private Date ModifiedOn;
	
	@Column(name="deleted_by")
	private Long DeletedBy;
	
	@Column(name="deleted_on")
	private Date DeletedOn;
	
	@NotNull
	@Column(name="is_delete")
	private boolean IsDelete;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String Token) {
		token = Token;
	}

	public Date getExpiredOn() {
		return ExpiredOn;
	}

	public void setExpiredOn(Date expiredOn) {
		ExpiredOn = expiredOn;
	}

	public boolean isExpired() {
		return isExpired;
	}

	public void setExpired(boolean isExpired) {
		this.isExpired = isExpired;
	}

	public String getUsedFor() {
		return usedFor;
	}

	public void setUsedFor(String usedFor) {
		this.usedFor = usedFor;
	}

	public Long getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Long createdBy) {
		CreatedBy = createdBy;
	}

	public Date getCreatedOn() {
		return CreatedOn;
	}

	public void setCreatedOn(Date createdOn) {
		CreatedOn = createdOn;
	}

	public Long getModified_by() {
		return modified_by;
	}

	public void setModified_by(Long modified_by) {
		this.modified_by = modified_by;
	}

	public Date getModifiedOn() {
		return ModifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		ModifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return DeletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		DeletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return DeletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		DeletedOn = deletedOn;
	}

	public boolean isIsDelete() {
		return IsDelete;
	}

	public void setIsDelete(boolean isDelete) {
		IsDelete = isDelete;
	}

	
	
}
